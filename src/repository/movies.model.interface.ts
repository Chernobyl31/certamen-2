export default interface MoviesModel {
    title: string, 
    usGross: number, 
    worldWideGross: number, 
    usDVDSales: number | null, 
    productionBudget: number, 
    releaseDate: string, 
    mpaaRating: string, 
    runningTimeMin: number | null, 
    distributor: string, 
    source: string, 
    majorGenre: string, 
    creativeType: string, 
    director: string, 
    rottenTomatoesRating: number, 
    imdbRating: number, 
    imdbVotes: number
}